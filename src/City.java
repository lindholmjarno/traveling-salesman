/* Luokka mallintaa koordinaattipisteen */

public class City {

    int x;
    int y;

    public City(int x, int y){
        this.x = x;
        this.y = y;
    }
    public String toString(){
        return "("+ x + "," +y + ")";
    }
}
