import java.util.ArrayList;

public class GreedyAlgorithm {
    // Aloituskaupunki
    int city = 0;
    double travelledDistance = 0;
    private TravelingSalesman ts;

    //Algoritmi tarvitsee kauppamatkaa-olion
    public GreedyAlgorithm(TravelingSalesman ts) {
        this.ts = ts;
    }

    public void Run() {
        double closestCity;
        int index= 0;
        int temp;
        ArrayList<Integer> cities = new ArrayList<>();
        System.out.println("Start city: 0\n");
        // Käydään läpi kaikki kaupungit -1 koska viimeinen lisätään loopin ulkopuolella
        for (int j = 0; j < ts.cityAmount-1; j++) {

            closestCity = Double.MAX_VALUE; //Aloitusmatka on suurin mahdollinen, jotta löydetään varmasti pienempi matka arraysta

            // Käydään läpi kaikki kaupungin mahdolliset reitit
            // Ohitetaan solut joissa 0.0 (käyty jo tai kotikaupunki)
            // otetaan matka ja kaupungin index talteen
            for (int i = 0; i < ts.distances.length; i++) {
                double dist = ts.distances[i][city];
                if (dist != 0.0) {
                    if (dist < closestCity) {
                        closestCity = dist;
                        index = i;
                    }
                }
            }

            travelledDistance += closestCity;
            cities.add(index);
            // Kun lyhin reitti selvillä, tyhjennetään array kyseisen kaupungin kohdalta
            for (int i = 0; i < ts.distances.length; i++) {
                ts.distances[i][city] = 0.0;
                ts.distances[city][i] = 0.0;
            }

            temp = city;
            city = index;
            System.out.println("Shortest route: " + temp + " -> " +index+ " (" +Math.floor(closestCity) +")");
            System.out.println("Travelled distance: " + Math.floor(travelledDistance) + "\n");
        }

        System.out.println("To Start city -> "+Math.floor(ts.distancesFromStartCity[city]));
        System.out.print("\nMatkustetut kaupungit: 0 -> ");

        for (int t : cities) {
            System.out.print(t+" -> ");
        }


        System.out.print("0 \n");

        travelledDistance += ts.distancesFromStartCity[city];

        System.out.println(Math.floor(travelledDistance));
    }
}
