
public class Main {

    public static void main(String[] args) {

        TravelingSalesman ts = new TravelingSalesman(4);
        GreedyAlgorithm gd = new GreedyAlgorithm(ts);
        gd.Run();
    }
}