import java.util.Random;
public class TravelingSalesman {

    Random rand = new Random();
    public City[] listOfCities;
    public double[][] distances;
    public int cityAmount;
    public double[] distancesFromStartCity;

    public TravelingSalesman(int cityAmount){
        listOfCities = new City[cityAmount];
        distances = new double[cityAmount][cityAmount];
        this.cityAmount = cityAmount;

        // Generoidaan kaupunkien sijainnit
        // Kaupunkien määrä kerrotaan 10, jotta vältyttäisiin päällekkäisiltä kaupungeilta
        for(int i = 0; i < cityAmount; i++){
            listOfCities[i] = new City(rand.nextInt(cityAmount*10),rand.nextInt(cityAmount*10));
        }
        //2d array kaupunkien välisille etäisyyksille
        for (int i = 0; i < distances.length; i++) {
            for (int j = 0; j < distances[i].length; j++) {
                if (i == j) {
                    distances[i][j] = 0;
                } else {
                    double ax,ay,bx,by;
                    ax = (double)listOfCities[i].x;
                    ay = (double)listOfCities[i].y;
                    bx = (double)listOfCities[j].x;
                    by = (double)listOfCities[j].y;
                    // AB2 = dx2 + dy2
                    distances[i][j] = Math.sqrt(Math.pow(ax-bx,2) + Math.pow(ay-by,2));
                }
            }
        }
        // Otetaan aloituskaupungin etäisyydet talteen,jotta lopussa löydetään
        // etäisyys viimeisestä kaupungista ensimmäiseen
        distancesFromStartCity = new double[distances.length];

        for (int i = 0; i < distances.length; i++) {
            distancesFromStartCity[i] = distances[i][0];
        }
    }
}
